package javarush.andreygoldyrev.cryptoanalyzer.utils;

import javarush.andreygoldyrev.cryptoanalyzer.exeptions.CryptoAnalyzerExeption;

import java.nio.file.Files;
import java.nio.file.Path;

import static javarush.andreygoldyrev.cryptoanalyzer.constants.Constants.ALPHABET;
import static javarush.andreygoldyrev.cryptoanalyzer.constants.Constants.TXT_FOLDER;

public class CryptoAnalyzerValidator {
    private static final String KEY_SHOULD_BE_A_NUMBER = "key should be a number";
    private static final String KEY_IS_OUT_OF_BOUNDS = String.format("key is out of bounds. key must be from 0 to %d", ALPHABET.length);
    private static final String FILE_SHOULD_HAVE_TXT_EXTENSION = "File should have .txt extension";
    private static final String SUFFIX = ".txt";
    private static boolean validated = true;

    public static boolean validateKey(String key) {
        int keyInt = 0;
        try {
            keyInt = Integer.parseInt(key);
        } catch (NumberFormatException e) {
            throw new CryptoAnalyzerExeption(KEY_SHOULD_BE_A_NUMBER, e);
        }
        if (keyInt < 0 || keyInt > ALPHABET.length) {
            validated = true;
            throw new CryptoAnalyzerExeption(KEY_IS_OUT_OF_BOUNDS);
        }
        return validated;
    }

    public static boolean fileSrcIsNotDest(String src, String dest) {
        if (src.equals(dest)) {
            validated = false;
        }
        return validated;
    }

    public static boolean validateExtension(String file) {

        if (!file.endsWith(SUFFIX)) {
            validated = false;
            throw new CryptoAnalyzerExeption(FILE_SHOULD_HAVE_TXT_EXTENSION);
        }
        return validated;
    }

    public static boolean validatePathExists(String file) {
        Path path = Path.of(TXT_FOLDER + file);
        if (!Files.exists(path)) {
            validated = false;

            throw new CryptoAnalyzerExeption(String.format("File %s not found", file));

        }
        return validated;
    }
}
