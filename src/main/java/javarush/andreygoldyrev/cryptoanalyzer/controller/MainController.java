package javarush.andreygoldyrev.cryptoanalyzer.controller;

import javarush.andreygoldyrev.cryptoanalyzer.model.CryptoAnalyzerData;
import javarush.andreygoldyrev.cryptoanalyzer.model.Result;
import javarush.andreygoldyrev.cryptoanalyzer.services.EncryptDecryptService;
import javarush.andreygoldyrev.cryptoanalyzer.services.ServiceFactory;

public class MainController {
    public Result performAction(CryptoAnalyzerData data) {
        EncryptDecryptService service = ServiceFactory.findService(data.getAction());
        Result result = service.execute(data);
        return result;
    }
}
