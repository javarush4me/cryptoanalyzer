package javarush.andreygoldyrev.cryptoanalyzer.utils;

import javarush.andreygoldyrev.cryptoanalyzer.exeptions.CryptoAnalyzerExeption;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static javarush.andreygoldyrev.cryptoanalyzer.constants.Constants.TXT_FOLDER;

public class DestFileCreator {
    public static void deleteOldCreateNewDestFile(String file){
        Path pathDest = Path.of(TXT_FOLDER + file);
        if (Files.exists(pathDest)) {
            try {
                Files.delete(pathDest);
                Files.createFile(pathDest);
            } catch (IOException e) {
                throw new CryptoAnalyzerExeption(e);
            }
        }
    }
}
