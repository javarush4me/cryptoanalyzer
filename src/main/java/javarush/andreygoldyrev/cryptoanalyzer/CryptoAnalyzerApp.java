package javarush.andreygoldyrev.cryptoanalyzer;

import javarush.andreygoldyrev.cryptoanalyzer.controller.MainController;
import javarush.andreygoldyrev.cryptoanalyzer.exeptions.CryptoAnalyzerExeption;
import javarush.andreygoldyrev.cryptoanalyzer.model.CryptoAnalyzerData;
import javarush.andreygoldyrev.cryptoanalyzer.model.Result;

import static javarush.andreygoldyrev.cryptoanalyzer.utils.CryptoAnalyzerValidator.*;

public class CryptoAnalyzerApp {
    private MainController mainController;

    public CryptoAnalyzerApp() {

        mainController = new MainController();
    }

    public Result run(String[] args) {
        long currentMills = System.currentTimeMillis();
        long startMills = currentMills;

        if (args.length == 3 || args.length == 4) {
            CryptoAnalyzerData data = new CryptoAnalyzerData(args[0], args[1], args[2]);
            if (args.length == 4) {
                data.setKey(args[3]);
                validateKey(data.getKey());
            }

            validateExtension(data.getSrcFile());
            validateExtension(data.getDestFile());
            validatePathExists(data.getSrcFile());
            fileSrcIsNotDest(data.getSrcFile(), data.getDestFile());

            Result result = mainController.performAction(data);

            currentMills = System.currentTimeMillis();
            System.out.println("Время выполнения в миллисекундах: " + (currentMills - startMills));

            return result;
        }
        throw new CryptoAnalyzerExeption();
    }
}
