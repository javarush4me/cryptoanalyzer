package javarush.andreygoldyrev.cryptoanalyzer.exeptions;

public class CryptoAnalyzerExeption extends RuntimeException{
    public CryptoAnalyzerExeption() {
    }

    public CryptoAnalyzerExeption(String message) {
        super(message);
    }

    public CryptoAnalyzerExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public CryptoAnalyzerExeption(Throwable cause) {
        super(cause);
    }
}
