package javarush.andreygoldyrev.cryptoanalyzer.services;

import javarush.andreygoldyrev.cryptoanalyzer.algorithm.CesarEcrypterDecrypter;
import javarush.andreygoldyrev.cryptoanalyzer.algorithm.EncryptDecrypt;
import javarush.andreygoldyrev.cryptoanalyzer.model.CryptoAnalyzerData;
import javarush.andreygoldyrev.cryptoanalyzer.model.Result;
import javarush.andreygoldyrev.cryptoanalyzer.model.ResultCode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static javarush.andreygoldyrev.cryptoanalyzer.constants.Constants.ENCRYPTING_COMPLETE;
import static javarush.andreygoldyrev.cryptoanalyzer.constants.Constants.TXT_FOLDER;
import static javarush.andreygoldyrev.cryptoanalyzer.utils.DestFileCreator.deleteOldCreateNewDestFile;

public class EncryptService implements EncryptDecryptService {

    private EncryptDecrypt cesarEncryptor = CesarEcrypterDecrypter.getInstance();

    @Override
    public Result execute(CryptoAnalyzerData data) {

        File fileSrc = new File(TXT_FOLDER + data.getSrcFile());
        File fileDest = new File(TXT_FOLDER + data.getDestFile());

        deleteOldCreateNewDestFile(data.getDestFile());

        try (LineIterator it = FileUtils.lineIterator(fileSrc, StandardCharsets.UTF_8.name())) {
            while (it.hasNext()) {
                String line = it.nextLine();
                String encryptedLine = cesarEncryptor.encrypt(line, Integer.parseInt(data.getKey()));
                try {
                    FileUtils.write(fileDest, encryptedLine, StandardCharsets.UTF_8, true);
                } catch (IOException e) {
                    return new Result(e.getMessage(), ResultCode.ERROR);
                }
            }
        } catch (IOException e) {
            return new Result(e.getMessage(), ResultCode.ERROR);
        }
        return new
                Result(ENCRYPTING_COMPLETE, ResultCode.OK);
    }
}