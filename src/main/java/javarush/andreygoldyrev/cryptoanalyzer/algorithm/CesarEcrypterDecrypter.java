package javarush.andreygoldyrev.cryptoanalyzer.algorithm;

import javarush.andreygoldyrev.cryptoanalyzer.constants.Constants;
import javarush.andreygoldyrev.cryptoanalyzer.exeptions.CryptoAnalyzerExeption;

import java.util.Map;

import static javarush.andreygoldyrev.cryptoanalyzer.constants.Constants.ALPHABET;

public class CesarEcrypterDecrypter implements EncryptDecrypt {
    private static final CesarEcrypterDecrypter CESAR_ECRYPTER_DECRYPTER = new CesarEcrypterDecrypter();
    public static final String NO_SUCH_CHARACTER_IN_THE_ALPHABET = "No such character in the Alphabet";

    private CesarEcrypterDecrypter() {
    }

    public static CesarEcrypterDecrypter getInstance() {
        return CESAR_ECRYPTER_DECRYPTER;
    }

    @Override
    public String encrypt(String text, int key) {

        return process(text, key, false);
    }

    @Override
    public String decrypt(String text, int key) {

        return process(text, -key, true);
    }

    private String process(String text, int key, boolean needToStop) {
        StringBuilder result = new StringBuilder();
        Map<Character, Integer> alphabetMap = Constants.getAlphabetMap();
        for (int i = 0; i < text.length(); i++) {
            Character originalCharacter = Character.toLowerCase(text.charAt(i));
            if (alphabetMap.containsKey(originalCharacter)) {
                int originalCharIndex = alphabetMap.get(originalCharacter);
                int newCharIndex = (ALPHABET.length + (originalCharIndex + key)) % ALPHABET.length;
                result.append(ALPHABET[newCharIndex]);
            } else if (needToStop) {
                throw new CryptoAnalyzerExeption(NO_SUCH_CHARACTER_IN_THE_ALPHABET + originalCharacter);
            }
        }
        return result.toString();
    }

}

