package javarush.andreygoldyrev.cryptoanalyzer.model;

public class CryptoAnalyzerData {
    private String action;
    private String srcFile;
    private String destFile;
    private String key;

    public CryptoAnalyzerData(String action, String srcFile, String destFile) {
        this.action = action;
        this.srcFile = srcFile;
        this.destFile = destFile;
    }

    public CryptoAnalyzerData(String action, String srcFile, String destFile, String key) {
        this.action = action;
        this.srcFile = srcFile;
        this.destFile = destFile;
        this.key = key;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSrcFile() {
        return srcFile;
    }

    public void setSrcFile(String srcFile) {
        this.srcFile = srcFile;
    }

    public String getDestFile() {
        return destFile;
    }

    public void setDestFile(String destFile) {
        this.destFile = destFile;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
