package javarush.andreygoldyrev.cryptoanalyzer.services;

import javarush.andreygoldyrev.cryptoanalyzer.model.CryptoAnalyzerData;
import javarush.andreygoldyrev.cryptoanalyzer.model.Result;

public interface EncryptDecryptService {
    Result execute (CryptoAnalyzerData data);
}
