# cryptoAnalyzer

Run PicocliRunner with parameters:
command, source file, destination file, key(if needed);

example:

encrypt dict.txt encryptedText.txt 3                - to encrypt txt file

decrypt encryptedText.txt decryptedText.txt 3       - to decrypt txt file with the given key

bruteforce encryptedText.txt bruteforcedText.txt    - to crack encrypted txt file with brute force method

App is working with russian alphabet. If you want to add more lalnguages you need to add them in the Constants class.

Source and destination files are stored in the texts/ folder.

