package javarush.andreygoldyrev.cryptoanalyzer.services;

import javarush.andreygoldyrev.cryptoanalyzer.exeptions.CryptoAnalyzerExeption;

public enum ServiceFactory {
    ENCRYPT(new EncryptService()),
    DECRYPT(new DecryptService()),
    BRUTEFORCE(new BruteForceService());
    public final EncryptDecryptService service;

    ServiceFactory(EncryptDecryptService encryptDecryptService) {
        this.service = encryptDecryptService;
    }
    public static EncryptDecryptService  findService(String serviceName){
        try {
            ServiceFactory serviceFactory = ServiceFactory.valueOf(serviceName.toUpperCase());
            return serviceFactory.service;
        } catch (IllegalArgumentException e) {
            throw new CryptoAnalyzerExeption(e);
        }
    }
}
