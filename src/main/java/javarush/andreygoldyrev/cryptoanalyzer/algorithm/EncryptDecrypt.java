package javarush.andreygoldyrev.cryptoanalyzer.algorithm;

import javarush.andreygoldyrev.cryptoanalyzer.model.Result;

public interface EncryptDecrypt {
    String encrypt(String text, int key);
    String decrypt(String text, int key);
}
