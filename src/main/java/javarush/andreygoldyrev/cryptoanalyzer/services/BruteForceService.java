package javarush.andreygoldyrev.cryptoanalyzer.services;

import javarush.andreygoldyrev.cryptoanalyzer.algorithm.CesarEcrypterDecrypter;
import javarush.andreygoldyrev.cryptoanalyzer.algorithm.EncryptDecrypt;
import javarush.andreygoldyrev.cryptoanalyzer.model.CryptoAnalyzerData;
import javarush.andreygoldyrev.cryptoanalyzer.model.Result;
import javarush.andreygoldyrev.cryptoanalyzer.model.ResultCode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static javarush.andreygoldyrev.cryptoanalyzer.constants.Constants.*;
import static javarush.andreygoldyrev.cryptoanalyzer.utils.DestFileCreator.deleteOldCreateNewDestFile;

public class BruteForceService implements EncryptDecryptService {

    private EncryptDecrypt cesarDecryptor = CesarEcrypterDecrypter.getInstance();

    @Override
    public Result execute(CryptoAnalyzerData data) {

        File fileSrc = new File(TXT_FOLDER + data.getSrcFile());
        File fileDest = new File(TXT_FOLDER + data.getDestFile());

        deleteOldCreateNewDestFile(data.getDestFile());

        int counter = 0;
        int counterMax = 0;
        String temp = "";
        String result = "";
        StringBuilder stringBuilder = new StringBuilder();

        try (LineIterator it = FileUtils.lineIterator(fileSrc, StandardCharsets.UTF_8.name())) {
            while (it.hasNext()) {
                String line = it.nextLine();
                stringBuilder.append(line);
            }
            for (int i = 0; i < ALPHABET.length; i++) {
                temp = cesarDecryptor.decrypt(stringBuilder.toString(), i);
                for (int j = 0; j < temp.length() - 1; j++) {
                    if (("" + temp.charAt(j) + temp.charAt(j + 1)).equals(", ")) {
                        counter++;
                    }
                }
                if (counter > counterMax) {
                    counterMax = counter;
                    result = temp;
                }
                counter = 0;
            }
        } catch (IOException e) {
            return new Result(e.getMessage(), ResultCode.ERROR);
        }
        try {
            FileUtils.write(fileDest, result, StandardCharsets.UTF_8, true);
        } catch (IOException e) {
            return new Result(e.getMessage(), ResultCode.ERROR);
        }
        return new
                Result(BRUTE_FORCE_COMPLETE, ResultCode.OK);

    }
}
