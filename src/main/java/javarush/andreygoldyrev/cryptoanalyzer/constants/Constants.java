package javarush.andreygoldyrev.cryptoanalyzer.constants;

import java.util.HashMap;
import java.util.Map;

public class Constants {
    public static final char[] ALPHABET = {'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з',
            'и', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ',
            'ъ', 'ы', 'ь', 'э','ю', 'я',
            '.', ',', '«', '»', '"', '\'', ':', '!', '?', ' ', '-', ':', ';', '(', ')', '*', '–','[',']', 'x','v','i', '\n', '\t',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
    public static final String TXT_FOLDER = "texts\\";
    public static final String BRUTE_FORCE_COMPLETE = "Brute force complete";
    public static final String DECRYPTING_COMPLETE = "Decrypting complete";
    public static final String ENCRYPTING_COMPLETE = "Encrypting complete";

    public static final Map<Character, Integer> ALPHA_MAP = new HashMap<>();

    public static Map<Character, Integer> getAlphabetMap() {
        if (ALPHA_MAP.isEmpty()) {
            for (int i = 0; i < ALPHABET.length; i++) {
                ALPHA_MAP.put(ALPHABET[i], i);
            }
            return ALPHA_MAP;
        }
        return ALPHA_MAP;
    }


}
